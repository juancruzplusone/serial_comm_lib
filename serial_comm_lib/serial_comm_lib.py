import serial
import queue
import threading
import time


class SerialCommLib:
    """
    This class is a serial communication library for connecting, sending, and receiving data.
    """

    def __init__(self, port, baud):
        try:
            self.ser = serial.Serial(port=port, baudrate=baud)
            self.inputQueue = queue.Queue()
            self.isListening = False
            self.listenThread = None
        except Exception as e:
            raise Exception("Error initializing serial communication") from e

    def send(self, data):
        """
        This method sends the specified data over the serial port.
        """
        try:
            if not self.ser.is_open:
                self.ser.open()
            self.ser.write(data.encode())  # Convert string to bytes and send
        except Exception as e:
            raise Exception("Error sending data") from e

    def listen(self):
        """
        This method starts a new thread that listens for data from the serial port.
        """
        self.isListening = True
        self.listenThread = threading.Thread(target=self._listenThread)
        self.listenThread.start()

    def stopListening(self):
        """
        This method stops the listening thread.
        """
        self.isListening = False
        if self.listenThread:
            self.listenThread.join()  # Wait for the thread to finish

    def getIncomingData(self):
        """
        This method returns all data received since the last call to this method.
        """
        data = []
        while not self.inputQueue.empty():
            data.append(self.inputQueue.get())
        return data

    def _listenThread(self):
        """
        This method runs in a separate thread, listening for data from the serial port.
        """
        while self.isListening:
            if self.ser.is_open and self.ser.in_waiting:
                line = self.ser.readline().decode().strip()  # Read a line and decode from bytes to string
                self.inputQueue.put(line)  # Add the line to the queue

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.stopListening()
        if self.ser.is_open:
            self.ser.close()

    def __del__(self):
        self.stopListening()
        if self.ser.is_open:
            self.ser.close()
